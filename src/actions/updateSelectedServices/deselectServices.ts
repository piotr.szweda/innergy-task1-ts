import { PriceList, ServiceType } from '../../model'
import { getServicesToDeselect } from './getServicesToDeselect'

export const deselectServices = (priceList: PriceList, previouslySelectedServices: ServiceType[], service: ServiceType) => {
    const toDeselect = getServicesToDeselect(priceList, previouslySelectedServices, service)
    return previouslySelectedServices.filter(s => toDeselect.indexOf(s) === -1)
}