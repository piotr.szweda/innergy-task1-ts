import { PriceList, ServiceType } from '../../model'

export const getServicesToDeselect = (priceList: PriceList, previouslySelectedServices: ServiceType[], service: ServiceType) => {
    const extraItems = priceList.filter(i => i.itemType === 'extra')
    const relatedService = extraItems.find(i => i.services[1] === service)?.services[0]
    const relatedServiceExistsAndSelected = relatedService !== undefined && previouslySelectedServices.indexOf(relatedService) !== -1
    if (relatedServiceExistsAndSelected) {
        const selectedMainServiceCount = previouslySelectedServices
            .filter(s => extraItems.some(e => e.services[1] === s && e.services[0] === relatedService))
            .length
        return selectedMainServiceCount > 1
            ? [service]
            : selectedMainServiceCount === 1
            ? [service, relatedService]
            : []
    }
    return previouslySelectedServices.indexOf(service) === -1
        ? []
        : [service]
}