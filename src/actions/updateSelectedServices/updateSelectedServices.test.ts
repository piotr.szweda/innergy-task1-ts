import { priceList } from '../../data/test'
import { updateSelectedServices } from './updateSelectedServices'

describe("updateSelectedServices.select", () => {
    test("should select when not selected", () => {
        const result = updateSelectedServices(priceList, [], { type: "Select", service: "Photography" });
        expect(result).toEqual(["Photography"]);
    });

    test("should not select the same service twice", () => {
        const result = updateSelectedServices(priceList, ["Photography"], { type: "Select", service: "Photography" });
        expect(result).toEqual(["Photography"]);
    });

    test("should not select related service when main service is not selected", () => {
        const result = updateSelectedServices(priceList, ["WeddingSession"], { type: "Select", service: "BlurayPackage" });
        expect(result).toEqual(["WeddingSession"]);
    });

    test("should select related service when main service is selected", () => {
        const result = updateSelectedServices(priceList, ["WeddingSession", "VideoRecording"], {
            type: "Select",
            service: "BlurayPackage"
        });
        expect(result).toEqual(["WeddingSession", "VideoRecording", "BlurayPackage"]);
    });

    test("should select related service when one of main services is selected", () => {
        const result = updateSelectedServices(priceList, ["WeddingSession", "Photography"], {
            type: "Select",
            service: "TwoDayEvent"
        });
        expect(result).toEqual(["WeddingSession", "Photography", "TwoDayEvent"]);
    });
});

describe("updateSelectedServices.deselect", () => {
    test("should deselect", () => {
        const result = updateSelectedServices(priceList, ["WeddingSession", "Photography"], {
            type: "Deselect",
            service: "Photography"
        });
        expect(result).toEqual(["WeddingSession"]);
    });

    test("should do nothing when service not selected", () => {
        const result = updateSelectedServices(priceList, ["WeddingSession", "Photography"], {
            type: "Deselect",
            service: "TwoDayEvent"
        });
        expect(result).toEqual(["WeddingSession", "Photography"]);
    });

    test("should deselect related when last main service deselected", () => {
        const result = updateSelectedServices(priceList, ["WeddingSession", "Photography", "TwoDayEvent"], {
            type: "Deselect",
            service: "Photography"
        });
        expect(result).toEqual(["WeddingSession"]);
    });

    test("should not deselect related when at least one main service stays selected", () => {
        const result = updateSelectedServices(priceList, ["WeddingSession", "Photography", "VideoRecording", "TwoDayEvent"], {
            type: "Deselect",
            service: "Photography"
        });
        expect(result).toEqual(["WeddingSession", "VideoRecording", "TwoDayEvent"]);
    });
});