import { ServiceType } from '../../model'
import { PriceList } from '../../model/PriceList'

export const selectServices = (priceList: PriceList, previouslySelectedServices: ServiceType[], service: ServiceType) => {
    if (previouslySelectedServices.indexOf(service) !== -1) {
        return [...previouslySelectedServices]
    } else {
        const relatedMainServices = priceList
            .filter(i => i.itemType === 'extra' && i.services[0] === service)
            .map(i => i.services[1])
        if (relatedMainServices.length > 0) {
            const isAnyMainServiceSelected = relatedMainServices.some(s => previouslySelectedServices.indexOf(s) !== -1)
            return isAnyMainServiceSelected
                ? [...previouslySelectedServices, service]
                : [...previouslySelectedServices]
        } else {
            return [...previouslySelectedServices, service]
        }
    }
}