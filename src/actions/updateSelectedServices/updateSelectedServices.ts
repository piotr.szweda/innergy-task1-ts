import { PriceList, ServiceType } from '../../model'
import { deselectServices } from './deselectServices'
import { selectServices } from './selectServices'

export const updateSelectedServices = (
    priceList: PriceList,
    previouslySelectedServices: ServiceType[],
    action: { type: "Select" | "Deselect"; service: ServiceType }
) => {
    switch (action.type) {
        case 'Deselect':
            return deselectServices(priceList, previouslySelectedServices, action.service)
        case 'Select':
            return selectServices(priceList, previouslySelectedServices, action.service)
        default:
            throw new Error(`Unsupported action ${action.type}.`)
    }
}