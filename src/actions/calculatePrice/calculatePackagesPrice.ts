import { PriceList, ServiceType, ServiceYear } from '../../model'

export const calculatePackagesPrice = (
    priceList: PriceList,
    selectedServices: ServiceType[],
    selectedYear: ServiceYear
) => priceList
    .filter(i =>
        i.itemType === 'package'
        && (i.year === undefined || i.year === selectedYear)
        && (i.services.every(s => selectedServices.indexOf(s) !== -1))
    ).reduce((p, c) => p + c.unitPrice, 0)
