import { PriceList, ServiceType, ServiceYear } from '../../model'

export const calculateBasePriceWithPromotion = (
    priceList: PriceList,
    selectedServices: ServiceType[],
    selectedYear: ServiceYear,
    selectedService: ServiceType
) => {
    const basePrice = priceList
        .find(i =>
            (i.itemType === 'base' || i.itemType === 'extra')
            && i.services[0] === selectedService
            && (i.year === undefined || i.year === selectedYear)
        )
        .unitPrice
    const isInPackage = priceList.some(i =>
        i.itemType === 'package'
        && i.services.indexOf(selectedService) !== -1
        && i.services.every(s => selectedServices.indexOf(s) !== -1))
    const finalPrice = isInPackage ? 0 : priceList
        .filter(i =>
            i.itemType === 'promotion'
            && i.services[0] === selectedService
            && (i.year === undefined || i.year === selectedYear)
            && i.services.some((s, i) => i > 0 && selectedServices.indexOf(s) !== -1)
        )
        .reduce((prev, curr) => Math.min(prev, curr.unitPrice), basePrice)
    return { basePrice, finalPrice }
}