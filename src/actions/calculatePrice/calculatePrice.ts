import { PriceList, ServiceType, ServiceYear } from '../../model'
import { calculateBasePriceWithPromotion } from './calculateBasePriceWithPromotion'
import { calculatePackagesPrice } from './calculatePackagesPrice'

export const calculatePrice = (priceList: PriceList, selectedServices: ServiceType[], selectedYear: ServiceYear) => {
    const basePricesWithPromotions = selectedServices.map(selectedService =>
        calculateBasePriceWithPromotion(priceList, selectedServices, selectedYear, selectedService)
    )
    const packagesPrice = calculatePackagesPrice(priceList, selectedServices, selectedYear)
    return basePricesWithPromotions.reduce((p, c) => ({
        basePrice: p.basePrice += c.basePrice,
        finalPrice: p.finalPrice += c.finalPrice
    }), { basePrice: 0, finalPrice: packagesPrice })
}