import { PriceListItemType } from './PriceListItemType'
import { ServiceType } from './ServiceType'
import { ServiceYear } from './ServiceYear'

export interface PriceListItem {
    itemType: PriceListItemType
    services: ServiceType[]
    unitPrice: number
    year?: ServiceYear
}