import { PriceListItem, PriceListItemType, ServiceType, ServiceYear } from '../../model'

const priceListItem = (
    itemType: PriceListItemType, services: ServiceType[], unitPrice: number, year?: ServiceYear
) => (<PriceListItem>{ itemType, services, year, unitPrice })

export const priceList = [
    priceListItem('base', ['Photography'], 1700, 2020),
    priceListItem('base', ['Photography'], 1800, 2021),
    priceListItem('base', ['Photography'], 1900, 2022),
 
    priceListItem('base', ['VideoRecording'], 1700, 2020),
    priceListItem('base', ['VideoRecording'], 1800, 2021),
    priceListItem('base', ['VideoRecording'], 1900, 2022),
 
    priceListItem('package', ['Photography', 'VideoRecording'], 2200, 2020),
    priceListItem('package', ['Photography', 'VideoRecording'], 2300, 2021),
    priceListItem('package', ['Photography', 'VideoRecording'], 2500, 2022),

    priceListItem('base', ['WeddingSession'], 600),
    priceListItem('promotion', ['WeddingSession', 'Photography'], 300),
    priceListItem('promotion', ['WeddingSession', 'Photography'], 0, 2022),
    priceListItem('promotion', ['WeddingSession', 'VideoRecording'], 300),

    priceListItem('extra', ['BlurayPackage', 'VideoRecording'], 300),
    priceListItem('extra', ['TwoDayEvent', 'Photography'], 400),
    priceListItem('extra', ['TwoDayEvent', 'VideoRecording'], 400)
]